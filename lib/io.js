/**
 * Open conection to mongodb
 */

var app = require('./app');
var dbmanager = require('./dbmanager');

var messages = dbmanager.db.collection('messages');
var io;

module.exports.init = function(server) {
  io = server;

  io.on('connection', function (socket) {

    var getMessages = function () {
      messages.find({}).sort( { timestamp: 1 } ).toArray(function(err, messages) {
        messages = messages.map(function (message) {
          if(typeof message.disabled == 'undefined'){
            message.disabled = false;
          }
          return message;
        });
        socket.emit('messages', { messages: messages });
      });
    };

    getMessages();


    socket.on('remove', function(data){
      messages.deleteOne({timestamp: data.timestamp}, function(err, result) {
        if (err) return next(err);
        socket.emit('delete-success');
      });
    });

    socket.on('getall', function(data){
      getMessages();
    });

    socket.on('edit', function(data){
      var ts = data.timestamp;
      delete data.timestamp;
      delete data._id;
      messages.updateOne({timestamp: ts}, {$set: data}, function(err, result) {
        if (err) return false;
        socket.emit('edit-success');
      });
    });

    socket.on('changeDisablity', function(data){
      var ts = data.timestamp;

      messages.updateOne({timestamp: ts}, {$set: {disabled: data.disabled}}, function(err, result) {
        if (err) return false;
        socket.emit('disable-success');
      });
    });

    socket.on('add', function(data){
      messages.save({
        message: data.message,
        code: data.code,
        codeType: data.codeType,
        imageUrl: data.imageUrl,
        disabled: false,
        timestamp: Number(new Date)
      }, function(err, result) {
        if (err) return false;
        socket.emit('add-success');
      });
    });
  });


};

module.exports.broadcastMessage = function(message) {
  io.sockets.emit('message', { message: message});
};
