var express = require('express');
var dbmanager = require('../dbmanager');
var io = require('../io');

var router = express.Router();
var messages = dbmanager.db.collection('messages');

var defaultRoute = function(req, res, next) {
  var env = req.param('env') || process.env.NODE_ENV;

  res.render('index', {
    env: env
  });

};

router.get('/', defaultRoute);
router.get('/edit', defaultRoute);

module.exports = router;
