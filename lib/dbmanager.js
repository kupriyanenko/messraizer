/**
 * Open conection to mongodb
 */

var mongodb = require('mongodb');
var client = mongodb.MongoClient;
var env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    db: 'mongodb://localhost/messraiser'
  },
  production: {
    db: process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || 'mongodb://localhost/messraiser'
  }
};

module.exports.init = function(callback) {
    client.connect(config[env].db, function(err, database) {
        if (err) return callback(err);

        module.exports.db = database;
        callback(null, database);
    });
};
