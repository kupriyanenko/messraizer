require.config({
    "baseUrl": "/javascripts/out/",
    "paths": {
        "underscore": "/javascripts/bower_components/underscore/underscore",
        "prism": "/javascripts/prism",
        "react": "/javascripts/bower_components/react/react",
        "page": "/javascripts/bower_components/page/page"
    },
    "shim": {
        "underscore": {
            "exports": "_"
        },
        "react": {
            "exports": "React"
        },
        "prism": {
            "exports": "Prism"
        }
    }
});

require([
    "page",
    "index",
    "editor"

], function (page, index, editor) {
    page('/', function() {
        index.start();
    });
    page('/edit', function() {
        editor.start();
    });
    page();
});
