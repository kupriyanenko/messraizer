var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0];


export default {
	get screenWidth() {
		return w.innerWidth || e.clientWidth || g.clientWidth;
	},
	get screenHeight() {
		return w.innerHeight|| e.clientHeight|| g.clientHeight;
	}
}
