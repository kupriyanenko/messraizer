require('bower').commands
  .install()
  .on('end', function(data) {
    if (data) {
      console.log(data);
    }
  })
  .on('error', function(err) {
    console.error(err);

    process.exit(1);
  });
